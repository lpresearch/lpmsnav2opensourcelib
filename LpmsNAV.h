/***********************************************************************
** Copyright (C) 2021 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
**
** This file is part of the Open Motion Analysis Toolkit (OpenMAT).
**
** Redistribution and use in source and binary forms, with
** or without modification, are permitted provided that the
** following conditions are met:
**
** Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** Redistributions in binary form must reproduce the above copyright
** notice, this list of conditions and the following disclaimer in
** the documentation and/or other materials provided with the
** distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***********************************************************************/
#ifndef LPMS_NAV_H
#define LPMS_NAV_H

#include <string>
#include <thread>
#include <mutex>
#include <queue>
#include <vector>
#include <cstdio>
#include <fstream>
#ifdef _WIN32
#define _USE_MATH_DEFINES
#else
#include <algorithm>
#endif
#include <math.h>
#include <time.h>
#include "LpmsNAVI.h"
#include "util.h"
#include "SerialPort.h"
#include "MicroMeasure.h"


///////////////////////////////////////
// Register command
///////////////////////////////////////
#define REPLY_ACK                           0
#define REPLY_NACK                          1 

// Firmware update and in-application-programmer upload
#define UPDATE_FIRMWARE                     2
#define UPDATE_IAP                          3

// Sensor info
#define GET_FIRMWARE_VERSION                4   // 16bit
#define GET_HARDWARE_VERSION                5   // 16bit
#define GET_SERIAL_NUMBER                   6   

// Configuration and status
#define GET_CONFIG                          7
#define GET_STATUS                          8

// | format | baudrate | output rate | deg/rad type | 180/360 output | initial mode |
// Mode switching
#define GOTO_COMMAND_MODE                   9
#define GOTO_STREAM_MODE                    10

// Sensor data
#define GET_SENSOR_DATA                     11

// Sensor initial mode
#define SET_STARTUP_STREAMING_MODE          12
#define SET_STARTUP_COMMAND_MODE            13

// 180/360 output
#define SET_360_OUTPUT                      14
#define SET_180_OUTPUT                      15

// deg/rad type
#define SET_DEG_OUTPUT                      16
#define SET_RAD_OUTPUT                      17

// Stream freq output rate 
#define GET_STREAM_FREQ                     18
#define SET_STREAM_FREQ                     19

// UART baudrate
#define GET_UART_BAUDRATE                   20
#define SET_UART_BAUDRATE                   21

//format
#define SET_UART_FORMAT                     22

// Reset
#define RESET_BIAS                          23
#define RESET_HEADING                       24
#define RESET_SENSOR                        25 // sensor hard reset to default settings

// LED Control
#define ENABLE_LED                          26
#define DISABLE_LED                         27

// Autocalibration
#define GET_ENABLE_GYRO_AUTOCAL             28
#define SET_ENABLE_GYRO_AUTOCAL             29

#define GET_IAP_STATUS                      30
#define GET_SENSOR_MODEL                    31

#define GET_SENSOR_ID                       32
#define SET_SENSOR_ID                       33

#define GET_STARTUP_STATIC_TIME             34
#define SET_STARTUP_STATIC_TIME             35

#define GET_FILTER_VERSION                  36

///////////////////////////////////////
// Config register
///////////////////////////////////////
#define SENSOR_CONFIG_MODE_MASK                         0x1
#define SENSOR_CONFIG_COMMAND_MODE                      0x0
#define SENSOR_CONFIG_STREAMING_MODE                    0x1

#define SENSOR_CONFIG_OUTPUT_MASK                       0x1 << 1
#define SENSOR_CONFIG_180_OUTPUT                        0x0 << 1
#define SENSOR_CONFIG_360_OUTPUT                        0x1 << 1

#define SENSOR_CONFIG_TYPE_MASK                         0x1 << 2
#define SENSOR_CONFIG_TYPE_DEG                          0x0 << 2
#define SENSOR_CONFIG_TYPE_RAD                          0x1 << 2

#define SENSOR_CONFIG_OUTPUT_RATE_MASK_PRE010           (uint32_t)0x3 << 3
#define SENSOR_CONFIG_OUTPUT_RATE_10HZ_ENABLE_PRE010    (uint32_t)0x0 << 3
#define SENSOR_CONFIG_OUTPUT_RATE_25HZ_ENABLE_PRE010    (uint32_t)0x1 << 3
#define SENSOR_CONFIG_OUTPUT_RATE_50HZ_ENABLE_PRE010    (uint32_t)0x2 << 3
#define SENSOR_CONFIG_OUTPUT_RATE_100HZ_ENABLE_PRE010   (uint32_t)0x3 << 3

#define SENSOR_CONFIG_OUTPUT_RATE_MASK                  (uint32_t)0xF << 11
#define SENSOR_CONFIG_OUTPUT_RATE_10HZ_ENABLE           (uint32_t)0x0 << 11
#define SENSOR_CONFIG_OUTPUT_RATE_25HZ_ENABLE           (uint32_t)0x1 << 11
#define SENSOR_CONFIG_OUTPUT_RATE_50HZ_ENABLE           (uint32_t)0x2 << 11
#define SENSOR_CONFIG_OUTPUT_RATE_100HZ_ENABLE          (uint32_t)0x3 << 11
#define SENSOR_CONFIG_OUTPUT_RATE_250HZ_ENABLE          (uint32_t)0x4 << 11
#define SENSOR_CONFIG_OUTPUT_RATE_500HZ_ENABLE          (uint32_t)0x5 << 11

#define SENSOR_CONFIG_OUTPUT_RATE_10HZ                  (uint32_t)10
#define SENSOR_CONFIG_OUTPUT_RATE_25HZ                  (uint32_t)25
#define SENSOR_CONFIG_OUTPUT_RATE_50HZ                  (uint32_t)50
#define SENSOR_CONFIG_OUTPUT_RATE_100HZ                 (uint32_t)100
#define SENSOR_CONFIG_OUTPUT_RATE_250HZ                 (uint32_t)250
#define SENSOR_CONFIG_OUTPUT_RATE_500HZ                 (uint32_t)500

#define SENSOR_CONFIG_UART_BAUDRATE_MASK                (uint32_t)0x07 << 5
#define SENSOR_CONFIG_UART_BAUDRATE_4800_ENABLE         (uint32_t)0x00 << 5
#define SENSOR_CONFIG_UART_BAUDRATE_9600_ENABLE         (uint32_t)0x01 << 5
#define SENSOR_CONFIG_UART_BAUDRATE_19200_ENABLE        (uint32_t)0x02 << 5
#define SENSOR_CONFIG_UART_BAUDRATE_28800_ENABLE        (uint32_t)0x03 << 5
#define SENSOR_CONFIG_UART_BAUDRATE_38400_ENABLE        (uint32_t)0x04 << 5
#define SENSOR_CONFIG_UART_BAUDRATE_57600_ENABLE        (uint32_t)0x05 << 5
#define SENSOR_CONFIG_UART_BAUDRATE_115200_ENABLE       (uint32_t)0x06 << 5
#define SENSOR_CONFIG_UART_BAUDRATE_4800                (uint32_t)4800
#define SENSOR_CONFIG_UART_BAUDRATE_9600                (uint32_t)9600
#define SENSOR_CONFIG_UART_BAUDRATE_19200               (uint32_t)19200
#define SENSOR_CONFIG_UART_BAUDRATE_28800               (uint32_t)28800
#define SENSOR_CONFIG_UART_BAUDRATE_38400               (uint32_t)38400
#define SENSOR_CONFIG_UART_BAUDRATE_57600               (uint32_t)57600
#define SENSOR_CONFIG_UART_BAUDRATE_115200              (uint32_t)115200

#define SENSOR_CONFIG_FORMAT_MASK                       0x03 << 8
#define SENSOR_CONFIG_FORMAT_INTEGER_ENABLE             0x00 << 8
#define SENSOR_CONFIG_FORMAT_FLOAT_ENABLE               0x01 << 8
#define SENSOR_CONFIG_FORMAT_ASCII_ENABLE               0x02 << 8

#define SENSOR_CONFIG_ENABLE_LED_MASK                   0x1 << 10
#define SENSOR_CONFIG_LED_DISABLE                       0x0 << 10
#define SENSOR_CONFIG_LED_ENABLE                        0x1 << 10

///////////////////////////////////////
// Other settings
///////////////////////////////////////
// Timeout settings (us)
#define TIMEOUT_IDLE            5000000 // 5 
#define SENSOR_DATA_QUEUE_SIZE  10
#define SAVE_DATA_LIMIT         3600000 // 3600*100*10 // 10hours

#define VERSION                 "2.0.0"

enum 
{
    WAIT_IGNORE,
    WAIT_FOR_ACKNACK,
    WAIT_FOR_DATA,
    // Sensor Info
    WAIT_FOR_SENSOR_MODEL,
    WAIT_FOR_HARDWARE_VERSION,
    WAIT_FOR_FIRMWARE_VERSION,
    WAIT_FOR_FILTER_VERSION,
    WAIT_FOR_SERIAL_NUMBER,
    WAIT_FOR_IAP_STATUS,

    // Sensor Settings
    WAIT_FOR_SENSOR_ID,
    WAIT_FOR_CONFIG,
    WAIT_FOR_STARTUP_STATIC_TIME,
    WAIT_FOR_SENSOR_DATA,
    WAIT_FOR_GYRO_GAIN,
    WAIT_FOR_STREAM_FREQ,
    WAIT_FOR_GYRO_AUTOCAL

};

///////////////////////////////////////
// LPMod bus
///////////////////////////////////////

#define BYTE_START              0x3A
#define BYTE_END0               0x0D
#define BYTE_END1               0x0A
#define LPPACKET_MAX_BUFFER     512
enum Protocol 
{
    // Protocol
    PACKET_START = 0,
    PACKET_ADDRESS,
    PACKET_FUNCTION,
    PACKET_INDEX,
    PACKET_LENGTH,
    PACKET_RAW_DATA,
    PACKET_LRC_CHECK,
    PACKET_END_LB,
    PACKET_END_HB,
};

struct LPPacket
{
    int rxState;
    uint16_t address;
    uint16_t index;
    uint16_t length;
    uint16_t function;
    uint8_t data[LPPACKET_MAX_BUFFER];
    uint16_t rawDataIndex;
    uint16_t chksum;
    uint16_t cs;


    LPPacket()
    {
        reset();
    }

    void reset()
    {
        rxState = PACKET_START;
        address = 0;
        index = 0;
        length = 0;
        function = 0;
        rawDataIndex = 0;
        chksum = 0;
        memset(data, 0, LPPACKET_MAX_BUFFER);
    }

};

struct SensorCommand
{
    uint8_t command;
    union Data {
        uint32_t i[64];
        float f[64];
        double d[32];
        unsigned char c[256];
    } data;
    int dataLength;

    int expectedResponse;
    int retryCount;
    bool sent;
    bool processed;
    bool error;
    long long commandDelay; //us

    SensorCommand(uint8_t cmd = 0, int response = WAIT_FOR_ACKNACK)
    {
        command = cmd;
        dataLength = 0;
        memset(data.c, 0, 256);
        expectedResponse = response;
        retryCount = 0;
        processed = false;
        sent = false;
        error = false;
        commandDelay = 10000;
    }

};


class LpmsNAV : public LpmsNAVI
{
    const static int INCOMING_DATA_MAX_LENGTH = 2048;
    const std::string TAG;

    std::string getVersion() {return VERSION;};
public:
    LpmsNAV(void);
#ifdef _WIN32
    LpmsNAV(int portno, int baudrate=115200);
#else
    LpmsNAV(std::string portno, int baudrate=115200);
#endif

    ~LpmsNAV(void);
	
    void release() { delete this; };
    void setSensorType(int type);
    void setPCBaudrate(int baud);
#ifdef _WIN32
    void setPCPort(int port);
#else
    void setPCPort(std::string port);
#endif
    int connect(void);
    bool disconnect(void);
    bool isConnected(void);

    // Sensor related
    bool hasInfo();
    LpmsNAVInfo getSensorInfo(void);

    bool resetHeading(void);
    bool resetGyroStaticBias(void);
    float getDataFrequency(void);

    bool setBaudrate(int baud);
    int getBaudrate(void);

    bool setOutputRange(int range);
    int getOutputRange(void);

    bool setStreamFrequency(int freq);
    int getStreamFrequency(void);
    
    bool enableLed(void);
    bool disableLed(void);

    int hasData(void);
    bool getSensorData(LpmsNAVData& sd);

    void setSensorDataQueueSize(unsigned int size);
    int getSensorDataQueueSize(void);

    bool sendGetSensorDataCommand();

    // > V0.0.5 API
    bool setAutoCalStatus(bool enable);
    bool getAutoCalStatus();
    bool setSensorID(int i);
    bool getSensorID();
    bool setStartUpStaticTime(float dt);
    bool getStartUpStaticTime();


    bool gotoCommandMode(void);
    bool gotoStreamingMode(void);
    bool getConfigRegister(void);
    bool resetFactory(void);

    bool getSensorSettings(bool clearQueue = false);

    void sendCommand(uint8_t cmd, uint8_t length, uint8_t* data);
    void sendRawCommand(unsigned char* cmd, int n);

    //data saving
    void startDataSaving(bool debugOutput=false);
    void stopDataSaving();
    void saveDataToFile(std::string filename);
    int getSavedDataCount();
    LpmsNAVData getSavedData(unsigned int i);


    void handleSaveData(const LpmsNAVData &sd, const uint16_t mode);

    // debug
    void setVerbose(int b);
    void printSensorInfo();
    bool isPre010Firmware();
    bool is01xFirmware();

    // reconnect
    bool getAutoReconnect(void);
    bool setAutoReconnect(bool value);
    long long getTimeoutThreshold(void);
    long long setTimeoutThreshold(long long value);

private:
    void init();
    void updateData();
    bool assertConnected();
    bool parseModbusByte(int n);
    void addSensorData(LpmsNAVData &data);
    void clearSensorDataQueue();
    bool parseData(const LPPacket& p);
    void parseConfigRegister(const uint8_t *dataBuffer);
    int parseBaudrate(uint32_t config);
    int parseStreamFreq(uint32_t config);
    int parseOutputRange(uint32_t config);
    bool parseLedEnable(uint32_t config);

    void addCommandQueue(SensorCommand cmd);
    void clearCommandQueue();

private:    
    // Serial Communication
    Serial sp;
#ifdef _WIN32
    int portno;
#else
    std::string portno;
#endif
    int baudrate;
    int mType;

    // General
    bool isDataPause_RS485;
    int  isVerbose;

    // Internal thread
    std::thread* t;
    bool isThreadRunning;
    bool isStopThread;
    bool autoReconnect;
    long long timeoutThreshold;

    // Protocol
    // LPBus
    LPPacket packet;
    std::mutex mLock;

    // Sensor
    LpmsNAVInfo sensorInfo;
    int sensorStatus;
    std::string errMsg;
    bool hasNewSensorInfo;

    // Sensor Data
    unsigned char incomingData[INCOMING_DATA_MAX_LENGTH];
    int16_t angle;
    int16_t rate;
    int16_t x_acc;
    int16_t y_acc;
    int16_t z_acc;
    uint8_t check_sum;
    LpmsNAVData sensorData;
    std::queue<LpmsNAVData> sensorDataQueue;
    unsigned int sensorDataQueueSize;

    // Stats
    float incomingDataRate;
    unsigned int lastTimestamp;
    unsigned int timestampCount;
    float dt;
    MicroMeasure mmDataFreq;
    MicroMeasure mmDataIdle;
    MicroMeasure mm;

    // command queue
    std::mutex mLockCommandQueue;
    std::queue<SensorCommand> commandQueue;
    MicroMeasure mmCommandTimer;
    long long lastSendCommandTime;

    // data saving
    bool saveDebugOutput;
    bool isDataSaving;
    std::ofstream myfile;
    std::vector<LpmsNAVData> savedDataBuffer;
    int savedDataCount;
    int freqCount;
    std::string currentFilename;

    // Protocol
    doubleArray2char da2c;
    floatArray2char fa2c;
    cArray2intArray c2i;
    double2char d2c;
    float2char  f2c;
    int2char i2c;

    // Firmware update
    int uploadPacketSize;
    float filePercent;
    bool sensorUpdating;
    bool isCancelFileUpload;

    // Pre 0.0.4 Firmware
    int lastSensorAngleSign;
    int revCount;
    std::vector<std::string> pre010FirmwareList;

};

#endif
