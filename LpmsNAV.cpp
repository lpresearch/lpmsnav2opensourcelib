#include "LpmsNAV.h"

using namespace std;
#ifdef _WIN32
LpmsNAVI* APIENTRY LpmsNAVFactory(int portno, int baudrate)
#else
LpmsNAVI* LpmsNAVFactory(std::string portno, int baudrate)
#endif
{
    return (LpmsNAVI*) new LpmsNAV(portno, baudrate);
}

LpmsNAV::LpmsNAV() :
    TAG("LpmsNAV"),
#ifdef _WIN32
    portno(1),
#else
    portno("/dev/ttyUSB0"),
#endif
    baudrate(115200),
    isVerbose(VERBOSE_NONE),
    sensorDataQueueSize(SENSOR_DATA_QUEUE_SIZE)
{
    t = NULL;

    // reserve space for data saving
    savedDataBuffer.reserve(SAVE_DATA_LIMIT);

    pre010FirmwareList.push_back("0.0.1");
    pre010FirmwareList.push_back("0.0.2");
    pre010FirmwareList.push_back("0.0.3");
    pre010FirmwareList.push_back("0.0.4");
    pre010FirmwareList.push_back("0.0.5");
    init();

    autoReconnect = false;
    timeoutThreshold = TIMEOUT_IDLE;
}

#ifdef _WIN32
LpmsNAV::LpmsNAV(int portno, int baudrate) :
#else
LpmsNAV::LpmsNAV(std::string portno, int baudrate) :
#endif
    TAG("LpmsNAV"),
    portno(portno),
    baudrate(baudrate),
    isVerbose(VERBOSE_NONE),
    sensorDataQueueSize(SENSOR_DATA_QUEUE_SIZE)
{
    t = NULL;
    // reserve space for data saving
    savedDataBuffer.reserve(SAVE_DATA_LIMIT);
    pre010FirmwareList.push_back("0.0.1");
    pre010FirmwareList.push_back("0.0.2");
    pre010FirmwareList.push_back("0.0.3");
    pre010FirmwareList.push_back("0.0.4");
    pre010FirmwareList.push_back("0.0.5");
    init();

    autoReconnect = false;
    timeoutThreshold = TIMEOUT_IDLE;
}

LpmsNAV::~LpmsNAV()
{
    if (myfile.is_open())
        myfile.close();
    if (sensorStatus != STATUS_DISCONNECTED &&
        sensorStatus != STATUS_CONNECTION_ERROR)
        disconnect();
}

void LpmsNAV::setSensorType(int type)
{
    mType = type;
    if (isVerbose) logd(TAG, "SensorType: %d\n", mType);
}

void LpmsNAV::setPCBaudrate(int baud)
{
    baudrate = baud;
    if (isVerbose) logd(TAG, "Baudrate: %d\n", baudrate);
}
#ifdef _WIN32
void LpmsNAV::setPCPort(int port)
#else
void LpmsNAV::setPCPort(std::string port)
#endif
{
    portno = port;
}

int LpmsNAV::connect()
{
    if (sensorStatus == STATUS_CONNECTING ||
        sensorStatus == STATUS_CONNECTED ||
        sensorStatus == STATUS_UPDATING)
    {
        logd(TAG, "Another connection established\n");
        return false;
    }

    init();
    t = new std::thread(&LpmsNAV::updateData, this);
    this_thread::sleep_for(chrono::milliseconds(1500));
    return sensorStatus;

}

bool LpmsNAV::disconnect()
{
    incomingDataRate = 0;
    isStopThread = true;
    if (t != NULL)
        t->join();
    t = NULL;
    sp.close();
    sensorStatus = STATUS_DISCONNECTED;
    if (isVerbose) logd(TAG, "COM:%d disconnected\n", portno);
    return true;
}

bool LpmsNAV::isConnected()
{
    if (sensorStatus == STATUS_CONNECTED || sensorStatus == STATUS_UPDATING)
        return true;
    return false;
}

bool LpmsNAV::hasInfo()
{
    return hasNewSensorInfo;
}

LpmsNAVInfo LpmsNAV::getSensorInfo()
{
    hasNewSensorInfo = false;
    return sensorInfo;
}

bool LpmsNAV::resetHeading()
{
    if (!assertConnected())
        return false;

    if (isVerbose) logd(TAG, "Reset heading\n");

    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(RESET_HEADING, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));

    return true;
}

bool LpmsNAV::resetGyroStaticBias(void)
{
    if (!assertConnected())
        return false;
    if (isVerbose) logd(TAG, "Reset bias\n");
    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(RESET_BIAS, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));

    return true;
}

float LpmsNAV::getDataFrequency()
{
    return 1.0f / incomingDataRate;
}

bool LpmsNAV::setBaudrate(int baud)
{
    if (!assertConnected())
        return false;
    int2char i2c;
    i2c.int_val = baud;
    if (isVerbose) logd(TAG, "setBaudrate\n");

    SensorCommand cmd1;
    cmd1.command = SET_UART_BAUDRATE;
    cmd1.dataLength = 4;
    cmd1.data.i[0] = i2c.int_val;
    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(cmd1);
    addCommandQueue(SensorCommand(GET_CONFIG, WAIT_FOR_CONFIG));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}

int LpmsNAV::getBaudrate()
{
    return sensorInfo.baudrate;
}

bool LpmsNAV::setOutputRange(int range)
{
    if (!assertConnected())
        return false;
    if (range != 180 && range != 360)
        return false;
    if (isVerbose) logd(TAG, "setOutputRange\n");

    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand((range == 180) ? SET_180_OUTPUT : SET_360_OUTPUT, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(GET_CONFIG, WAIT_FOR_CONFIG));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}

int LpmsNAV::getOutputRange()
{
    return sensorInfo.outputRange;
}

bool LpmsNAV::setStreamFrequency(int freq)
{
    if (!assertConnected())
        return false;
    int2char i2c;
    i2c.int_val = freq;
    if (isVerbose) logd(TAG, "setStreamFrequency\n");

    SensorCommand cmd1;
    cmd1.command = SET_STREAM_FREQ;
    cmd1.dataLength = 4;
    cmd1.data.i[0] = i2c.int_val;
    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(cmd1);
    addCommandQueue(SensorCommand(GET_CONFIG, WAIT_FOR_CONFIG));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}

int LpmsNAV::getStreamFrequency()
{
    if (!assertConnected())
        return false;
    if (isVerbose) logd(TAG, "Get stream freq\n");

    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(GET_CONFIG, WAIT_FOR_CONFIG));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}

bool LpmsNAV::enableLed()
{
    if (!assertConnected())
        return false;
    if (isVerbose) logd(TAG, "enableLed\n");

    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(ENABLE_LED, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(GET_CONFIG, WAIT_FOR_CONFIG));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}

bool LpmsNAV::disableLed()
{
    if (!assertConnected())
        return false;
    if (isVerbose) logd(TAG, "disableLed\n");

    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(DISABLE_LED, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(GET_CONFIG, WAIT_FOR_CONFIG));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}

int LpmsNAV::hasData()
{
    if (!isConnected())
        return 0;
    return (int)sensorDataQueue.size();
}
bool LpmsNAV::getSensorData(LpmsNAVData& sd)
{
    if (!assertConnected())
        return false;
    mLock.lock();
    if (sensorDataQueue.empty())
    {
        mLock.unlock();
        return false;
    }
    sd = sensorDataQueue.front();
    sensorDataQueue.pop();
    mLock.unlock();

    return true;
}

void LpmsNAV::setSensorDataQueueSize(unsigned int size)
{
    sensorDataQueueSize = size;
    mLock.lock();
    while (sensorDataQueue.size() > sensorDataQueueSize)
    {
        sensorDataQueue.pop();
    }

    mLock.unlock();
}

int LpmsNAV::getSensorDataQueueSize()
{
    return sensorDataQueueSize;
}

bool LpmsNAV::sendGetSensorDataCommand()
{
    if (!assertConnected())
        return false;

    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(GET_SENSOR_DATA, WAIT_FOR_ACKNACK));
    return true;
}

bool LpmsNAV::setAutoCalStatus(bool enable)
{
    if (!assertConnected())
        return false;

    int2char i2c;
    if (enable) {
        i2c.int_val = LPMS_ENABLE_GYRO_AUTOCAL;
    }
    else {
        i2c.int_val = LPMS_DISABLE_GYRO_AUTOCAL;
    }

    SensorCommand cmd1;
    cmd1.command = SET_ENABLE_GYRO_AUTOCAL;
    cmd1.dataLength = 4;
    cmd1.data.i[0] = i2c.int_val;
    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(cmd1);
    addCommandQueue(SensorCommand(GET_ENABLE_GYRO_AUTOCAL, WAIT_FOR_GYRO_AUTOCAL));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}

bool LpmsNAV::getAutoCalStatus()
{
    if (!assertConnected())
        return false;

    if (isVerbose) logd(TAG, "getAutoCalStatus\n");

    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(GET_ENABLE_GYRO_AUTOCAL, WAIT_FOR_GYRO_AUTOCAL));
    addCommandQueue(SensorCommand(GET_CONFIG, WAIT_FOR_CONFIG));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    
    return true;
}

bool LpmsNAV::setSensorID(int i)
{
    if (!assertConnected())
        return false;
    int2char i2c;
    i2c.int_val = i;
    if (isVerbose) logd(TAG, "setSensorID\n");

    SensorCommand cmd1;
    cmd1.command = SET_SENSOR_ID;
    cmd1.dataLength = 4;
    cmd1.data.i[0] = i2c.int_val;
    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(cmd1);
    addCommandQueue(SensorCommand(GET_SENSOR_ID, WAIT_FOR_SENSOR_ID));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}

bool LpmsNAV::getSensorID()
{
    if (!assertConnected())
        return false;
    if (isVerbose) logd(TAG, "getSensorID\n");

    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(GET_SENSOR_ID, WAIT_FOR_SENSOR_ID));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}

bool LpmsNAV::setStartUpStaticTime(float dt)
{
    if (!assertConnected())
        return false;

    if (dt < 0.0f)
        dt = 0.0f;
    if (isVerbose) logd(TAG, "setStartUpStaticTime\n");

    SensorCommand cmd1;
    cmd1.command = SET_STARTUP_STATIC_TIME;
    cmd1.dataLength = 4;
    cmd1.data.f[0] = dt;

    // 0.1.x firmware uses integer data
    if (is01xFirmware())
    {
        cmd1.data.i[0] = (int)dt;
    }
    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(cmd1);
    addCommandQueue(SensorCommand(GET_STARTUP_STATIC_TIME, WAIT_FOR_STARTUP_STATIC_TIME));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}

bool LpmsNAV::getStartUpStaticTime()
{
    if (!assertConnected())
        return false;

    if (isVerbose) logd(TAG, "getStartUpStaticTime\n");

    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(GET_STARTUP_STATIC_TIME, WAIT_FOR_STARTUP_STATIC_TIME));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}




/// Internal

bool LpmsNAV::gotoCommandMode()
{
    if (!assertConnected())
        return false;
    if (isVerbose) logd(TAG, "Goto command mode\n");

    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    return true;
}

bool LpmsNAV::gotoStreamingMode()
{
    if (!assertConnected())
        return false;

    if (isVerbose) logd(TAG, "Goto streaming mode\n");
    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}

bool LpmsNAV::getConfigRegister()
{
    if (!assertConnected())
        return false;
    if (isVerbose) logd(TAG, "getConfigRegister\n");

    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(GET_CONFIG, WAIT_FOR_CONFIG));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}

bool LpmsNAV::resetFactory()
{
    if (!assertConnected())
        return false;
    if (isVerbose) logd(TAG, "Reset Factory\n");

    clearCommandQueue();
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(RESET_SENSOR, WAIT_FOR_ACKNACK));

    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    // Sensor Settings
    addCommandQueue(SensorCommand(GET_SENSOR_ID, WAIT_FOR_SENSOR_ID));
    addCommandQueue(SensorCommand(GET_CONFIG, WAIT_FOR_CONFIG));
    addCommandQueue(SensorCommand(GET_ENABLE_GYRO_AUTOCAL, WAIT_FOR_GYRO_AUTOCAL));
    addCommandQueue(SensorCommand(GET_STARTUP_STATIC_TIME, WAIT_FOR_STARTUP_STATIC_TIME));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
    return true;
}


bool LpmsNAV::getSensorSettings(bool clearQueue)
{
    if (!assertConnected())
        return false;

    if (clearQueue)
        clearCommandQueue();

    // Sensor Info
    addCommandQueue(SensorCommand(GOTO_COMMAND_MODE, WAIT_FOR_ACKNACK));
    addCommandQueue(SensorCommand(GET_SENSOR_MODEL, WAIT_FOR_SENSOR_MODEL));
    addCommandQueue(SensorCommand(GET_FIRMWARE_VERSION, WAIT_FOR_FIRMWARE_VERSION));
    addCommandQueue(SensorCommand(GET_FILTER_VERSION, WAIT_FOR_FILTER_VERSION));
    addCommandQueue(SensorCommand(GET_SERIAL_NUMBER, WAIT_FOR_SERIAL_NUMBER));
    addCommandQueue(SensorCommand(GET_IAP_STATUS, WAIT_FOR_IAP_STATUS));

    // Sensor Settings
    addCommandQueue(SensorCommand(GET_SENSOR_ID, WAIT_FOR_SENSOR_ID));
    addCommandQueue(SensorCommand(GET_CONFIG, WAIT_FOR_CONFIG));
    addCommandQueue(SensorCommand(GET_ENABLE_GYRO_AUTOCAL, WAIT_FOR_GYRO_AUTOCAL));
    addCommandQueue(SensorCommand(GET_STARTUP_STATIC_TIME, WAIT_FOR_STARTUP_STATIC_TIME));
    addCommandQueue(SensorCommand(GOTO_STREAM_MODE, WAIT_FOR_ACKNACK));
   
    return true;
}

bool LpmsNAV::getAutoReconnect()
{
    return autoReconnect;
}

bool LpmsNAV::setAutoReconnect(bool value)
{
    autoReconnect = value;

    if (autoReconnect)
        mmDataIdle.reset();

    return autoReconnect;
}

long long LpmsNAV::getTimeoutThreshold(void)
{
    return timeoutThreshold;
}

long long LpmsNAV::setTimeoutThreshold(long long value)
{
    if (sensorStatus == STATUS_DISCONNECTED)
        timeoutThreshold = abs(value);
    else
        logd(TAG, "Please disconnect sensor and try again.\n");
    return timeoutThreshold;
}

///////////////////////////
// Private
///////////////////////////
void LpmsNAV::init()
{
    // Internal thread
    isThreadRunning = false;
    isStopThread = true;
    mm.reset();

    // Protocol
    packet.reset();

    // Sensor
    sensorStatus = STATUS_DISCONNECTED;
    errMsg = "";

    hasNewSensorInfo = false;
    sensorInfo.reset();

    // Sensor Data
    clearSensorDataQueue();
    memset(incomingData, 0, INCOMING_DATA_MAX_LENGTH);

    // stats
    incomingDataRate = 0;
    lastTimestamp = 0;
    timestampCount = 0;
    mmDataFreq.reset();
    mmDataIdle.reset();

    // command queue
    mLockCommandQueue.lock();
    while (!commandQueue.empty())
    {
        commandQueue.pop();
    }
    mLockCommandQueue.unlock();
    mmCommandTimer.reset();
    lastSendCommandTime = 0;

    // File upload
    uploadPacketSize = 0;
    filePercent = 0.0;
    isCancelFileUpload = false;
    sensorUpdating = false;
    isDataSaving =false;
    lastSensorAngleSign = 1;
    revCount = 0;
    saveDebugOutput = false;
}

void LpmsNAV::updateData()
{

    int reconnectCount = 0;

    // Main thread loop to connect and read sensor data
    do
    {
        sensorStatus = STATUS_CONNECTING;
        isStopThread = false;

        // Connect Sensor
        if (sp.open(portno, baudrate))
        {
            if (isVerbose) 
            {
#ifdef _WIN32
                logd(TAG, "COM:%d connection established\n", portno);
#else
                logd(TAG, "COM:%s connection established\n", portno.c_str());
#endif
            }
        }
        else
        {
                stringstream ss;
                errMsg = ss.str();
#ifdef _WIN32
                ss << "Error connecting to port: " << portno << "@" << baudrate;
#else
                ss << "Error connecting to port: " << portno.c_str() << "@" << baudrate;
#endif 
                errMsg = ss.str();
                sensorStatus = STATUS_CONNECTION_ERROR;
        }

        // Serial connection established
        if (sensorStatus != STATUS_CONNECTION_ERROR)
        {
            // Initialize 
            int count = 0;
            int readResult = 0;
            int TDRRetryCount = 0;
            packet.reset();

            clearSensorDataQueue();
            incomingDataRate = 0;
            lastTimestamp = 0;
            isDataSaving = false;

            sensorStatus = STATUS_CONNECTED;
            mmDataFreq.reset();
            mmDataIdle.reset();
            mmCommandTimer.reset();


            getSensorSettings();

            while (!isStopThread)
            {
                // read data
                if (!sp.isConnected())
                {
                    loge(TAG, "Connection interrupted\n");
                    sensorStatus = STATUS_CONNECTION_ERROR;
                    break;
                }

                // Only check data timeout if autoreconnect is enabled
                if (autoReconnect && mmDataIdle.measure() > timeoutThreshold) // 5 secs no data
                {
                    errMsg = "Data timeout";
                    sensorStatus = STATUS_DATA_TIMEOUT;
                    break;
                }

                // Process command queue
                // Send command
                mLockCommandQueue.lock();
                if (!commandQueue.empty() && mmCommandTimer.measure() > commandQueue.front().commandDelay)
                {
                    if (commandQueue.front().processed)
                    {
                        commandQueue.pop();
                    }
                    else
                    {
                        if (!commandQueue.front().sent)
                        {
                            SensorCommand cmd = commandQueue.front();
                            sendCommand(cmd.command, cmd.dataLength, cmd.data.c);
                            commandQueue.front().sent = true;
                            mmCommandTimer.reset();
                        }
                        // Sent but no feedback
                        else if (commandQueue.front().expectedResponse == WAIT_IGNORE)
                        {
                            commandQueue.front().processed = true;
                        }
                    }
                }
                mLockCommandQueue.unlock();


                // Read data
                isThreadRunning = true;
                readResult = sp.readData(incomingData, INCOMING_DATA_MAX_LENGTH);

                // parse data
                if (readResult > 0) {
                    mmDataIdle.reset();
                    parseModbusByte(readResult);
                }

                this_thread::sleep_for(chrono::milliseconds(1));

            }
        }

        if (isStopThread)
            sensorStatus = STATUS_DISCONNECTED;

        // Clean up serial
        if (sp.isConnected())
        {
            sp.close();
        }
        if (isVerbose)
        {
#ifdef _WIN32
            logd(TAG, "COM:%d disconnected\n", portno);
#else
            logd(TAG, "COM:%s disconnected\n", portno.c_str());
#endif
        }

        // Deinit
        isThreadRunning = false;

        // Autoreconnect
        if (autoReconnect && !isStopThread)
        {
            reconnectCount++;
            if (isVerbose)
                logd(TAG, "reconnecting %d\n", reconnectCount);
            this_thread::sleep_for(chrono::milliseconds(1000));
        }
    
    } while (autoReconnect && !isStopThread);


    t = NULL;
}

bool LpmsNAV::assertConnected()
{
    if (!isConnected())
    {
        if (isVerbose) loge(TAG, "Sensor not connected\n");
        return false;
    }
    return true;
}

bool LpmsNAV::parseModbusByte(int n)
{
    unsigned char b;
    for (int i = 0; i<n; ++i)
    {
        b = incomingData[i];

        switch (packet.rxState) {
        case PACKET_START:
            if (b == BYTE_START) {
                packet.rxState = PACKET_FUNCTION;
                packet.rawDataIndex = 0;
                packet.cs = 0;
            }
            break;
        
            //function
        case PACKET_FUNCTION:
            packet.function = b;
            packet.rxState = PACKET_INDEX;
            break;

            //index
        case PACKET_INDEX:
            packet.index = b;
            packet.rxState = PACKET_LENGTH;
            break;

            //length
        case PACKET_LENGTH:
            packet.length = b;

            if (packet.length > 0)
                packet.rxState = PACKET_RAW_DATA;
            else
                packet.rxState = PACKET_LRC_CHECK;
            
            if (packet.length > LPPACKET_MAX_BUFFER)
                packet.rxState = PACKET_START;
            break;

            //data
        case PACKET_RAW_DATA:
            packet.data[packet.rawDataIndex++] = b;
            if (packet.rawDataIndex == packet.length)
                packet.rxState = PACKET_LRC_CHECK;
            break;

            //chcksum
        case PACKET_LRC_CHECK:
            packet.chksum = b;
            packet.rxState = PACKET_END_LB;
            break;

        case PACKET_END_LB:
            if (b == BYTE_END0)
                packet.rxState = PACKET_END_HB;
            else
                packet.rxState = PACKET_START;
            break;

        case PACKET_END_HB:
            if (b == BYTE_END1)
            {
                //Verify checksum
                //check_sum = id + function + index + length;
                uint8_t check_sum = packet.function + packet.index + packet.length;
                for (int i = 0; i < packet.length; ++i)
                    check_sum += packet.data[i];

                if (check_sum == packet.chksum)
                {
                    parseData(packet);
                }

            }
            packet.rxState = PACKET_START;
            break;

        default:
            packet.rxState = PACKET_START;
            //return false;
            break;
        }
    }
    return true;
}


bool LpmsNAV::parseData(const LPPacket& p)
{
    switch (p.function)
    {
    case REPLY_ACK:
        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
            if (commandQueue.front().expectedResponse == WAIT_FOR_ACKNACK)
                commandQueue.front().processed = true;
        }
        mLockCommandQueue.unlock();
        if (isVerbose == VERBOSE_DEBUG) logd(TAG, "Got ACK\n");
        break;

    case REPLY_NACK:
        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
            //if (commandQueue.front().expectedResponse == WAIT_FOR_ACKNACK)
            commandQueue.front().processed = true;
            commandQueue.front().error = true;
        }
        mLockCommandQueue.unlock();
        if (isVerbose == VERBOSE_DEBUG) logd(TAG, "Got NACK\n");
        break;

    case GET_CONFIG:
        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
            if (commandQueue.front().expectedResponse == WAIT_FOR_CONFIG)
                commandQueue.front().processed = true;
        }
        mLockCommandQueue.unlock();
        if (isVerbose) logd(TAG, "Got GET_CONFIG\n");
        parseConfigRegister(p.data);
        hasNewSensorInfo = true;
        break;

    case GET_FIRMWARE_VERSION:
        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
            if (commandQueue.front().expectedResponse == WAIT_FOR_FIRMWARE_VERSION)
                commandQueue.front().processed = true;
        }
        mLockCommandQueue.unlock();
        if (isVerbose) logd(TAG, "Got GET_FIRMWARE_VERSION\n");
        sensorInfo.firmwareVersion.assign((const char*)p.data, p.length);
        //checkPre004Firmware();
        hasNewSensorInfo = true;
        break;

    case GET_FILTER_VERSION:
        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
            if (commandQueue.front().expectedResponse == WAIT_FOR_FILTER_VERSION)
                commandQueue.front().processed = true;
        }
        mLockCommandQueue.unlock();
        if (isVerbose) logd(TAG, "Got GET_FILTER_VERSION\n");
        sensorInfo.filterVersion.assign((const char*)p.data, p.length);
        //checkPre004Firmware();
        hasNewSensorInfo = true;
        break;

    case GET_HARDWARE_VERSION:
        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
            if (commandQueue.front().expectedResponse == WAIT_FOR_HARDWARE_VERSION)
                commandQueue.front().processed = true;
        }
        mLockCommandQueue.unlock();
        if (isVerbose) logd(TAG, "Got GET_HARDWARE_VERSION\n");
        if (isVerbose) logd(TAG, "%d\n", p.length);
        sensorInfo.hardwareVersion.assign((const char*)p.data, p.length);
        hasNewSensorInfo = true;
        break;

    case GET_SERIAL_NUMBER:
        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
            if (commandQueue.front().expectedResponse == WAIT_FOR_SERIAL_NUMBER)
                commandQueue.front().processed = true;
        }
        mLockCommandQueue.unlock();
        if (isVerbose) logd(TAG, "Got GET_SERIAL_NUMBER\n");
        sensorInfo.serialNumber.assign((const char*)p.data, p.length);
        hasNewSensorInfo = true;
        break;

    case GET_IAP_STATUS:
        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
            if (commandQueue.front().expectedResponse == WAIT_FOR_IAP_STATUS)
                commandQueue.front().processed = true;
        }
        mLockCommandQueue.unlock();
        if (isVerbose == VERBOSE_DEBUG) logd(TAG, "Got GET_IAP_STATUS\n");
        memcpy(i2c.c, p.data, p.length);
        sensorInfo.iapStatus = i2c.int_val;
        hasNewSensorInfo = true;
        break;

    case GET_STREAM_FREQ:
        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
            if (commandQueue.front().expectedResponse == WAIT_FOR_STREAM_FREQ)
                commandQueue.front().processed = true;
        }
        mLockCommandQueue.unlock();
        if (isVerbose) logd(TAG, "Got GET_STREAM_FREQ\n");
        memcpy(i2c.c, p.data, p.length);
        sensorInfo.streamFreq = i2c.int_val;
        hasNewSensorInfo = true;
        break;

    case GET_ENABLE_GYRO_AUTOCAL:
        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
        if (commandQueue.front().expectedResponse == WAIT_FOR_GYRO_AUTOCAL)
            commandQueue.front().processed = true;
        }
        mLockCommandQueue.unlock();
        if (isVerbose == VERBOSE_DEBUG) logd(TAG, "Got GET_ENABLE_GYRO_AUTOCAL\n");
        memcpy(i2c.c, p.data, p.length);
        sensorInfo.autoCalStatus = (bool)i2c.int_val;
        hasNewSensorInfo = true;
        break;

    case GET_SENSOR_MODEL:
        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
            if (commandQueue.front().expectedResponse == WAIT_FOR_SENSOR_MODEL)
                commandQueue.front().processed = true;
        }
        mLockCommandQueue.unlock();

        if (isVerbose) logd(TAG, "Got GET_SENSOR_MODEL\n");
        sensorInfo.sensorModel.assign((const char*)p.data, p.length);
        hasNewSensorInfo = true;
        break;

    case GET_SENSOR_ID:
        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
            if (commandQueue.front().expectedResponse == WAIT_FOR_SENSOR_ID)
                commandQueue.front().processed = true;
        }
        mLockCommandQueue.unlock();
        if (isVerbose) logd(TAG, "Got GET_SENSOR_ID\n");
        memcpy(i2c.c, p.data, p.length);
        sensorInfo.sensorId = i2c.int_val;
        hasNewSensorInfo = true;
        break;

    case GET_STARTUP_STATIC_TIME:
        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
            if (commandQueue.front().expectedResponse == WAIT_FOR_STARTUP_STATIC_TIME)
                commandQueue.front().processed = true;
        }
        mLockCommandQueue.unlock();

        // 0.1.x firmware uses integer data
        if (is01xFirmware())
        {
            memcpy(i2c.c, p.data, p.length);
            sensorInfo.startUpStaticTime = (float)i2c.int_val;
        }
        else
        {
            memcpy(f2c.c, p.data, p.length);
            sensorInfo.startUpStaticTime = f2c.float_val;
        }

        if (isVerbose == VERBOSE_DEBUG) logd(TAG, "Got GET_STARTUP_STATIC_TIME %f\n", sensorInfo.startUpStaticTime);
        hasNewSensorInfo = true;
        break;

    case GET_SENSOR_DATA:

        mLockCommandQueue.lock();
        if (!commandQueue.empty())
        {
            if (commandQueue.front().expectedResponse == WAIT_FOR_SENSOR_DATA)
                commandQueue.front().processed = true;
        }
        mLockCommandQueue.unlock();

        memcpy(c2i.c, p.data, 10);
        //Scale and store data
        dt = float(mm.measure()) / 1000000.0f;
        incomingDataRate = 0.995f * incomingDataRate + 0.005f * dt;
        mm.reset();
        sensorData.timestamp = (float)timestampCount * sensorInfo.dt;
        timestampCount++;
        if (sensorInfo.outputRange == 360)
            sensorData.gAngle = (uint16_t)c2i.int_val[0] / 100.0f;
        else
            sensorData.gAngle = c2i.int_val[0] / 100.0f;

        sensorData.gRate = c2i.int_val[1] / 50.0f;
        sensorData.acc[0] = c2i.int_val[2] / 1000.0f;   //x_acc;
        sensorData.acc[1] = c2i.int_val[3] / 1000.0f;   //y_acc;
        sensorData.acc[2] = c2i.int_val[4] / 1000.0f;   //z_acc;

        freqCount++;
        freqCount %= 1;
        if (isDataSaving && freqCount == 0)
        {
            /*
            if (savedDataCount < SAVE_DATA_LIMIT)
            {
                mLock.lock();
                savedDataBuffer.push_back(sensorData);
                mLock.unlock();
                savedDataCount++;
            }
            */
            handleSaveData(sensorData, p.function);
        }
        mLock.lock();
        if (sensorDataQueue.size() < sensorDataQueueSize) {
            sensorDataQueue.push(sensorData);
        }
        else
        {
            sensorDataQueue.pop();
            sensorDataQueue.push(sensorData);
        }
        mLock.unlock();
        break;

    default:
        break;
    }

    return true;
}

void LpmsNAV::parseConfigRegister(const uint8_t *dataBuffer)
{
    uint32_t config;
    memcpy(&config, dataBuffer, 4);
    sensorInfo.baudrate = parseBaudrate(config);
    sensorInfo.streamFreq = parseStreamFreq(config);
    sensorInfo.dt = 1 / (float)sensorInfo.streamFreq;
    sensorInfo.outputRange = parseOutputRange(config);
    sensorInfo.ledEnabled = parseLedEnable(config);
}

void LpmsNAV::printSensorInfo()
{
    logd(TAG, "Sensor Model: %s\n", sensorInfo.sensorModel.c_str());
    logd(TAG, "Hardware version: %s\n", sensorInfo.hardwareVersion.c_str());
    logd(TAG, "Firmware version: %s\n", sensorInfo.firmwareVersion.c_str());
    logd(TAG, "Serial number: %s\n", sensorInfo.serialNumber.c_str());
    logd(TAG, "IAP status: %d\n", sensorInfo.iapStatus);
    logd(TAG, "Sensor ID: %d\n", sensorInfo.sensorId);
    logd(TAG, "Strem freq.: %d\n", sensorInfo.streamFreq);
    logd(TAG, "Baudrate: %d\n", sensorInfo.baudrate);
    logd(TAG, "Output range: %d\n", sensorInfo.outputRange);
    logd(TAG, "Led Enable: %s\n", sensorInfo.ledEnabled ? "True" : "False");
    logd(TAG, "Auto Calibration: %s\n", sensorInfo.autoCalStatus ? "True" : "False");
    logd(TAG, "StartUp Static Time: %s\n", sensorInfo.startUpStaticTime ? "True" : "False");
}

int LpmsNAV::parseBaudrate(uint32_t config)
{
    uint32_t baudrate;

    if ((config & SENSOR_CONFIG_UART_BAUDRATE_MASK) == SENSOR_CONFIG_UART_BAUDRATE_4800_ENABLE)
        baudrate = SENSOR_CONFIG_UART_BAUDRATE_4800;
    else if ((config & SENSOR_CONFIG_UART_BAUDRATE_MASK) == SENSOR_CONFIG_UART_BAUDRATE_9600_ENABLE)
        baudrate = SENSOR_CONFIG_UART_BAUDRATE_9600;
    else if ((config & SENSOR_CONFIG_UART_BAUDRATE_MASK) == SENSOR_CONFIG_UART_BAUDRATE_19200_ENABLE)
        baudrate = SENSOR_CONFIG_UART_BAUDRATE_19200;

    else if ((config & SENSOR_CONFIG_UART_BAUDRATE_MASK) == SENSOR_CONFIG_UART_BAUDRATE_28800_ENABLE)
        baudrate = SENSOR_CONFIG_UART_BAUDRATE_28800;

    else if ((config & SENSOR_CONFIG_UART_BAUDRATE_MASK) == SENSOR_CONFIG_UART_BAUDRATE_38400_ENABLE)
        baudrate = SENSOR_CONFIG_UART_BAUDRATE_38400;

    else if ((config & SENSOR_CONFIG_UART_BAUDRATE_MASK) == SENSOR_CONFIG_UART_BAUDRATE_57600_ENABLE)
        baudrate = SENSOR_CONFIG_UART_BAUDRATE_57600;

    else if ((config & SENSOR_CONFIG_UART_BAUDRATE_MASK) == SENSOR_CONFIG_UART_BAUDRATE_115200_ENABLE)
        baudrate = SENSOR_CONFIG_UART_BAUDRATE_115200;

    return baudrate;
}


int LpmsNAV::parseStreamFreq(uint32_t config)
{
    uint32_t mask = SENSOR_CONFIG_OUTPUT_RATE_MASK;
    if (isPre010Firmware())
    {
        mask = SENSOR_CONFIG_OUTPUT_RATE_MASK_PRE010;
        if ((config & mask) == SENSOR_CONFIG_OUTPUT_RATE_10HZ_ENABLE_PRE010)
            return SENSOR_CONFIG_OUTPUT_RATE_10HZ;
        else if ((config & mask) == SENSOR_CONFIG_OUTPUT_RATE_25HZ_ENABLE_PRE010)
            return SENSOR_CONFIG_OUTPUT_RATE_25HZ;
        else if ((config & mask) == SENSOR_CONFIG_OUTPUT_RATE_50HZ_ENABLE_PRE010)
            return SENSOR_CONFIG_OUTPUT_RATE_50HZ;
        else if ((config & mask) == SENSOR_CONFIG_OUTPUT_RATE_100HZ_ENABLE_PRE010)
            return SENSOR_CONFIG_OUTPUT_RATE_100HZ;
    }
    else
    {
        if ((config & mask) == SENSOR_CONFIG_OUTPUT_RATE_10HZ_ENABLE)
            return SENSOR_CONFIG_OUTPUT_RATE_10HZ;
        else if ((config & mask) == SENSOR_CONFIG_OUTPUT_RATE_25HZ_ENABLE)
            return SENSOR_CONFIG_OUTPUT_RATE_25HZ;
        else if ((config & mask) == SENSOR_CONFIG_OUTPUT_RATE_50HZ_ENABLE)
            return SENSOR_CONFIG_OUTPUT_RATE_50HZ;
        else if ((config & mask) == SENSOR_CONFIG_OUTPUT_RATE_100HZ_ENABLE)
            return SENSOR_CONFIG_OUTPUT_RATE_100HZ;
        else if ((config & mask) == SENSOR_CONFIG_OUTPUT_RATE_250HZ_ENABLE)
            return SENSOR_CONFIG_OUTPUT_RATE_250HZ;
        else if ((config & mask) == SENSOR_CONFIG_OUTPUT_RATE_500HZ_ENABLE)
            return SENSOR_CONFIG_OUTPUT_RATE_500HZ;
    }

    return SENSOR_CONFIG_OUTPUT_RATE_100HZ;
}

int LpmsNAV::parseOutputRange(uint32_t config)
{
    if ((config & SENSOR_CONFIG_OUTPUT_MASK) == SENSOR_CONFIG_180_OUTPUT)
        return 180;
    else if ((config & SENSOR_CONFIG_OUTPUT_MASK) == SENSOR_CONFIG_360_OUTPUT)
        return 360;
    return 0;
}

bool LpmsNAV::parseLedEnable(uint32_t config)
{
    if ((config & SENSOR_CONFIG_ENABLE_LED_MASK) == SENSOR_CONFIG_LED_ENABLE)
        return true;
    else if ((config & SENSOR_CONFIG_ENABLE_LED_MASK) == SENSOR_CONFIG_LED_DISABLE)
        return false;
    return 0;
}

void LpmsNAV::clearSensorDataQueue()
{
    mLock.lock();
    while (!sensorDataQueue.empty())
        sensorDataQueue.pop();
    mLock.unlock();
}


void LpmsNAV::addSensorData(LpmsNAVData &data)
{
    mLock.lock();
    if (sensorDataQueue.size() > sensorDataQueueSize) 
    {
        if (isVerbose) logd(TAG, "Sensor queue size full\n");
        sensorDataQueue.pop();
    }
    sensorDataQueue.push(data);
    mLock.unlock();
}

void LpmsNAV::sendCommand(uint8_t cmd, uint8_t length, uint8_t* data)
{
    uint8_t cmdBuffer[256];
    
    cmdBuffer[0] = 0x3A;
    int n = 1;
    cmdBuffer[2 - n] = cmd;
    cmdBuffer[3 - n] = length;

    if (length > 0)
    {
        memcpy(cmdBuffer + 4 - n, data, length);
    }

    //checksum
    uint8_t txLrcCheck = 0;
    for (int i = 1; i < length+4 - n; ++i)
        txLrcCheck += cmdBuffer[i];

    cmdBuffer[length + 4 - n] = txLrcCheck;
    cmdBuffer[length + 5 - n] = 0x0D;
    cmdBuffer[length + 6 - n] = 0x0A;
    sp.writeData((unsigned char*)cmdBuffer, length+7 - n);

   if (isVerbose == VERBOSE_DEBUG) 
   {
       printf("Send: %d\n", cmd);
       for (int i = 0; i < length + 7 - n; ++i)
       {
           printf("%X ", cmdBuffer[i]);
       }
       printf("\n");
   }
}

void LpmsNAV::sendRawCommand(unsigned char* cmd, int n)
{
    sp.writeData(cmd, n);
}


void LpmsNAV::startDataSaving(bool debugOutput)
{
    if (isDataSaving)
    {
        if (isVerbose) logd(TAG, "Data save action running\n");
        return;
    }
    isDataSaving = true;

    saveDebugOutput = debugOutput;

    currentFilename = "";
    mLock.lock();
    savedDataBuffer.clear();
    mLock.unlock();
    savedDataCount = 0;
    timestampCount = 0;
    if (myfile.is_open())
        myfile.close();
}

void LpmsNAV::stopDataSaving()
{
    isDataSaving = false;
    currentFilename = "";
    if (myfile.is_open())
        myfile.close();

}

void LpmsNAV::saveDataToFile(string filename)
{

}

int LpmsNAV::getSavedDataCount()
{
    return savedDataCount;
}

LpmsNAVData LpmsNAV::getSavedData(unsigned int i)
{
    LpmsNAVData d;
    if (i > savedDataBuffer.size())
        return d;
    d = savedDataBuffer[i];
    return d;
}

const string backupDir = "./log";
void LpmsNAV::handleSaveData(const LpmsNAVData &sd, const uint16_t mode)
{
    // Determine to create new file
    int fileLogDuration = 60;    //mins
    int min = getCurrentMin();

    if (currentFilename.empty())
    {
        currentFilename = backupDir + "/" + currentDateTime() + ".csv";
        savedDataCount = 0;
    }

    if (!myfile.is_open())
    {
        myfile.open(currentFilename, std::fstream::out | std::fstream::app);
        myfile << "Timestamp,Angle(deg),AngVel(deg/s),AccX(g),AccY(g),AccZ(g)\n";
    }

    myfile << sd.timestamp << "," << sd.gAngle << "," << sd.gRate << ","
        << sd.acc[0] << "," << sd.acc[1] << "," << sd.acc[2] ;

    myfile << "\n";


    savedDataCount++;
}

void LpmsNAV::setVerbose(int b) {
    isVerbose = b;
}

bool LpmsNAV::isPre010Firmware()
{
    vector<string>::iterator it = pre010FirmwareList.begin();
    while( it != pre010FirmwareList.end())
    {
        if (sensorInfo.firmwareVersion.find(*it) != std::string::npos)
        {
            return true;
        } 
        it++;
    }

    return false;
}

bool LpmsNAV::is01xFirmware()
{
    if (sensorInfo.firmwareVersion.find("NAV2-0.1.") != std::string::npos)
    {
        return true;
    }

    return false;
}

void LpmsNAV::addCommandQueue(SensorCommand cmd)
{
    mLockCommandQueue.lock();
    commandQueue.push(cmd);
    mLockCommandQueue.unlock();
}

void LpmsNAV::clearCommandQueue()
{
    mLockCommandQueue.lock();
    while (!commandQueue.empty())
        commandQueue.pop();
    mLockCommandQueue.unlock();
}
