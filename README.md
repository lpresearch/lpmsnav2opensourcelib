# LPMS-NAV2 Series OpenSource Lib


## Usage
### 1.Compiling Library programs
```bash
    $ cd ~
    $ git clone https://bitbucket.org/lpresearch/lpmsnav2opensourcelib
    $ cd lpmsnav2opensourcelib
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ make package
    $ sudo dpkg -i libLpmsNAV2_OpenSource-x.y.z-Linux.deb
```

### 2.Compiling Sample programs
```bash
    $ cd linux_example
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ ./LpmsNAVSimpleExample </dev/ttyXXX>
```
You might require running command using sudo if current user is not in dialout group. See [Troubleshooting](#troubleshooting) 

### 3.Compiling Sample programs
```bash
# Create ROS workspace.
    $ mkdir -p ~/catkin_ws/src
    $ cd ~/catkin_ws/src

# We suggest to create a symbolic link to ros_example folder instead of copying
    $ ln -s ~/lpmsnav2opensourcelib/ros_example lpms_nav2_ros_example

# Compiling ROS example programs
    $ cd ~/catkin_ws
    $ catkin_make
    $ source ./devel/setup.bash
```

Open a new terminal window and run roscore
```bash
    $ roscore
```

Run lpms_nav_node
```bash
    $ rosrun lpms_nav lpms_nav_node _port:=/dev/ttyUSB0 _baudrate:=115200
```

You can print out the imu data by subscribing to `/imu/data` topic
```bash
#Show imu message.
    $ rostopic echo /imu/data
#Plot imu message.
    $ rosrun rqt_plot rqt_plot
```

Alternatively, you can use the sample launch file (lpmsig1.launch) start data acquisition and data plotting:

```bash
roslaunch lpms_nav lpmsnav.launch
```

## ROS Package Summary

### 1. Supported Hardware
This driver interfaces with LPMS-NAV2 IMU sensor from LP-Research Inc.


### 2.1 lpms_nav_node
lpms_nav_node is a driver for the LPMS-NAV2 Inertial Measurement Unit. It publishes orientation, angular velocity, linear acceleration(covariances are not yet supported), and complies with the [Sensor message](https://wiki.ros.org/sensor_msgs) for [IMU API](http://docs.ros.org/api/sensor_msgs/html/msg/Imu.html) API.

#### 2.1.1 Published Topics
/imu/data ([sensor_msgs/Imu](http://docs.ros.org/api/sensor_msgs/html/msg/Imu.html)) 
:   Inertial data from the IMU. Includes calibrated acceleration, calibrated angular rates and orientation. The orientation is always unit quaternion. 

/imu/is_autocalibration_active ([std_msgs/Bool](http://docs.ros.org/api/std_msgs/html/msg/Bool.html), default: True)
:   Latched topic indicating if the gyro autocalibration feature is active

#### 2.1.2 Services
/imu/calibrate_gyroscope ([std_srvs/Empty](http://docs.ros.org/api/std_srvs/html/srv/Empty.html)) 
:   This service activates the IMU internal gyro bias estimation function. Please make sure the IMU sensor is placed on a stable platform with minimal vibrations before calling the service. Please make sure the sensor is stationary for at least 4 seconds. The service call returns a success response once the calibration procedure is completed.

/imu/reset_heading ([std_srvs/Empty](http://docs.ros.org/api/std_srvs/html/srv/Empty.html)) 
:   This service will reset the heading (yaw) angle of the sensor to zero. 

/imu/setAutoCalibrateStatus ([std_srvs/SetBool](http://docs.ros.org/melodic/api/std_srvs/html/srv/SetBool.html))
:   Turn on/off autocalibration function in the IMU. The status of autocalibration can be obtained by subscribing to the /imu/is_autocalibration_active topic. A message will published to /imu/is_autocalibration_active for each call to /imu/setAutoCalibrateStatus. 

/imu/send_getSensorData ([std_srvs/Empty](http://docs.ros.org/api/std_srvs/html/srv/Empty.html)) 
:   Poll imu sensor for latest sensor data. Latest sensor data will be publish to /imu/data topic once the driver received a new data from the sensor.

#### 2.1.3 Parameters

~port (string, default: /dev/ttyUSB0) 
:   The port the IMU is connected to.

~baudrate (int, default: 115200)
:   Baudrate for the IMU sensor.

~autoreconnect (bool, default True)
:   Enable/disable autoreconnect in library

~frame_id (string, default: imu) 
:   The frame in which imu readings will be returned.

~rate (int, default: 100) 
:   Data processing rate of the internal loop. This rate has to be equal or larger than the data streaming frequency of the sensor to prevent internal data queue overflow.

## Troubleshooting

### Serial connection error
this error might be due to the fact that the current user does not have sufficient permission to access the device. 
To allow access to sensors connected via USB, you need to ensure that the user has access to the /dev/ttyUSB devices. You can do this by adding the user to the dialout group. After this call, you should logout and login with this user to ensure the changed permissions are in effect.

```bash
    $ sudo adduser <username> dialout
```



For further information on `LP-Research`, please visit our website:

* http://www.lp-research.com

* http://www.alubi.cn
