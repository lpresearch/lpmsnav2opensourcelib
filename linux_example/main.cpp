#include <string>
#include <thread>
#include <iostream>

#include <cstdarg>

#include "lpsensor/LpmsNAVI.h"

using namespace std;
string TAG("Main");
LpmsNAVI* sensor1;
int g_sensorType;

std::thread *printThread;
static bool printThreadIsRunning = false;

void logd(std::string tag, const char* str, ...)
{
    va_list a_list;
    va_start(a_list, str);
    if (!tag.empty())
        printf("[%s] ", tag.c_str());
    vprintf(str, a_list);
    va_end(a_list);
}

void printTask(bool& quit)
{
    LpmsNAVData sd;
    
    if (g_sensorType == LPMS_NAV2_RS485)
        sensor1->sendGetSensorDataCommand();

    printThreadIsRunning = true;
    while (!quit && printThreadIsRunning && (sensor1->isConnected() || sensor1->getAutoReconnect()))
    {
        if (sensor1->hasData())
        {
            // get and print latest sensor data
            while (sensor1->hasData())
                sensor1->getSensorData(sd);

            logd(TAG, "t:%.2f angle: %.2f rate: %.2f Hz: %.2f\t\r", sd.timestamp, sd.gAngle, sd.gRate, sensor1->getDataFrequency());

            if (g_sensorType == LPMS_NAV2_RS485)
                    sensor1->sendGetSensorDataCommand();
            else
                this_thread::sleep_for(chrono::milliseconds(1));
        }
    }
    printThreadIsRunning = false;
    logd(TAG, "printTask terminated\n");
}

void printMenu()
{
    cout << "Main Menu" << endl;
    cout << "===================" << endl;
    cout << "[i] Print sensor info" << endl;
    cout << "[p] Print sensor data" << endl;
    cout << "[c] Go to command mode" << endl;
    cout << "[s] Go to streaming mode" << endl;
    cout << "[h] Reset sensor heading" << endl;
    cout << "[b] Calibrate gyro static bias" << endl;
    cout << "[e] Enable gyro autocalibration" << endl;
    cout << "[d] Disable gyro autocalibration" << endl;
    cout << "[r] Reset factory settings" << endl;
    cout << "[q] quit" << endl;
    cout << endl;
}

void printSensorInfo()
{

    // Print sensor info
    LpmsNAVInfo sensorInfo = sensor1->getSensorInfo();
    logd(TAG, "====================\n");
    logd(TAG, "Driver version: %s\n", sensor1->getVersion().c_str());
    logd(TAG, "Sensor model: %s\n", sensorInfo.sensorModel.c_str());
    logd(TAG, "Firmware version: %s\n", sensorInfo.firmwareVersion.c_str());
    logd(TAG, "Filter version: %s\n", sensorInfo.filterVersion.c_str());
    logd(TAG, "Serial number: %s\n", sensorInfo.serialNumber.c_str());
    logd(TAG, "Stream Freq: %d\n", sensorInfo.streamFreq);
    logd(TAG, "Baudrate: %d\n", sensorInfo.baudrate);
    logd(TAG, "Output range: %d\n", sensorInfo.outputRange);
    logd(TAG, "Gyro autocalibration: %s\n", sensorInfo.autoCalStatus? "Enabled": "Disabled");
    logd(TAG, "====================\n");
}

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        printf("Error command. Usage: CMD <COMPORT>\n");
        exit(0);
    }

#ifdef _WIN32
    int comportNo = atoi(argv[1]);
#else
    string comportNo(argv[1]);
#endif
    int baudrate = 115200;

    // Create LpmsNAV object with corresponding comport and baudrate
    sensor1 = LpmsNAVFactory(comportNo, baudrate);
    g_sensorType = LPMS_NAV2;
    sensor1->setSensorType(g_sensorType);
    
    // Cancel auto reconnection
    sensor1->setAutoReconnect(false);

    // Set timeout threshold (us, default = 5000000)
    sensor1->setTimeoutThreshold(1000000);

    // Connects to sensor
    if (sensor1->connect() != STATUS_CONNECTED)
    {
        logd(TAG, "Error connecting to sensor\n");
        sensor1->release();
        return 0;
    }

    printMenu();
    printSensorInfo();

    // Start print data thread
    bool quit = false;
    while (!quit)
    {
        char cmd;
        cin >> cmd;
        switch (cmd)
        {
        case 'i':
            printSensorInfo();
            break;

        case 'p': {     
            logd(TAG, "Print sensor data\n");       
            // Print sensor data
            if (!printThreadIsRunning)
                printThread = new std::thread(printTask, std::ref(quit));
            break;
        }

        case 'c':
            sensor1->gotoCommandMode();
            break;

        case 's':
            sensor1->gotoStreamingMode();
            break;

        case 'h':
            // reset sensor heading
            sensor1->resetHeading();
            break;

        case 'b':
            // recalibrate gyro
            sensor1->resetGyroStaticBias();
            break;

        case 'e':
            sensor1->setAutoCalStatus(true);
            break;

        case 'd':
            sensor1->setAutoCalStatus(false);
            break;

        case 'r':
            //send get sensor command
            sensor1->resetFactory();
            break;

        case 'q':
            // disconnect sensor
            sensor1->disconnect();
            quit = true;
            break;

        default:
            printThreadIsRunning = false;
            if (printThread && printThread->joinable()) {
                printThread->join();
                printThread = NULL;
            }
            printMenu();
            break;
        }
        this_thread::sleep_for(chrono::milliseconds(100));    
    }


    if (printThread && printThread->joinable())
        printThread->join();

    // release sensor resources
    sensor1->release();
    logd(TAG, "Bye\n");
}
