/***********************************************************************
** Copyright (C) 2021 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
**
** This file is part of the Open Motion Analysis Toolkit (OpenMAT).
**
** Redistribution and use in source and binary forms, with
** or without modification, are permitted provided that the
** following conditions are met:
**
** Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** Redistributions in binary form must reproduce the above copyright
** notice, this list of conditions and the following disclaimer in
** the documentation and/or other materials provided with the
** distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***********************************************************************/

#ifndef SERIALCLASS_H_INCLUDED
#define SERIALCLASS_H_INCLUDED

#define TIMEOUT_WAIT_TIME 5000000 // 5sec

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include "MicroMeasure.h"
#include "util.h"

#ifdef _WIN32
#include "windows.h"
#else
#include <stdio.h>      // standard input / output functions
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <cstring>
#endif

class Serial
{
    const std::string TAG;
public:
    enum  {
        BAUDRATE_4800=4800,
        BAUDRATE_9600=9600,
        BAUDRATE_19200=19200,
        BAUDRATE_38400=38400,
        BAUDRATE_57600=57600,
        BAUDRATE_115200=115200,
        BAUDRATE_230400=230400,
        BAUDRATE_460800=460800,
        BAUDRATE_921600=921600
    };
private:
    //Connection status
    bool connected;
   
    MicroMeasure mm;
#ifdef _WIN32
    //Serial comm handler
    HANDLE hSerial; 
    //Get various information about the connection
    COMSTAT status;
    //Keep track of last error
    DWORD errors;

    int portNo;

#else
    int fd;
    std::string portNo;
    int set_interface_attribs (int fd, int speed, int parity);
#endif

public:

    Serial();

    ~Serial(); 
   
#ifdef _WIN32
    bool open(int portno, int baudrate = BAUDRATE_9600);
#else
    bool open(std::string portno, int baudrate = BAUDRATE_115200);
#endif
    
    bool close();

    int readData(unsigned char *buffer, unsigned int nbChar);
    
    bool writeData(unsigned char *buffer, unsigned int nbChar);
    
    bool isConnected();

#ifdef _WIN32
    int getPortNo() { return portNo; }
#else
    std::string getPortNo() { return portNo; }
#endif  

};

#endif // SERIALCLASS_H_INCLUDED
