/***********************************************************************
** Copyright (C) 2021 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
**
** This file is part of the Open Motion Analysis Toolkit (OpenMAT).
**
** Redistribution and use in source and binary forms, with
** or without modification, are permitted provided that the
** following conditions are met:
**
** Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** Redistributions in binary form must reproduce the above copyright
** notice, this list of conditions and the following disclaimer in
** the documentation and/or other materials provided with the
** distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***********************************************************************/
#ifndef LPMS_NAV_I_H
#define LPMS_NAV_I_H

#ifdef _WIN32
#include "windows.h"
#endif

#include <string>

#define LPMS_NAV2                           0  //RS232,TTL
#define LPMS_NAV2_RS485                     1


// Sensor status
#define STATUS_DISCONNECTED                 0
#define STATUS_CONNECTING                   1
#define STATUS_CONNECTED                    2
#define STATUS_CONNECTION_ERROR             3
#define STATUS_DATA_TIMEOUT                 4
#define STATUS_UPDATING                     5

#define SENSOR_CONFIG_UART_BAUDRATE_4800    (uint32_t)4800
#define SENSOR_CONFIG_UART_BAUDRATE_9600    (uint32_t)9600
#define SENSOR_CONFIG_UART_BAUDRATE_19200   (uint32_t)19200
#define SENSOR_CONFIG_UART_BAUDRATE_28800   (uint32_t)28800
#define SENSOR_CONFIG_UART_BAUDRATE_38400   (uint32_t)38400
#define SENSOR_CONFIG_UART_BAUDRATE_57600   (uint32_t)57600
#define SENSOR_CONFIG_UART_BAUDRATE_115200  (uint32_t)115200

#define SENSOR_CONFIG_OUTPUT_RATE_10HZ      (uint32_t)10
#define SENSOR_CONFIG_OUTPUT_RATE_25HZ      (uint32_t)25
#define SENSOR_CONFIG_OUTPUT_RATE_50HZ      (uint32_t)50
#define SENSOR_CONFIG_OUTPUT_RATE_100HZ     (uint32_t)100
#define SENSOR_CONFIG_OUTPUT_RATE_250HZ     (uint32_t)250
#define SENSOR_CONFIG_OUTPUT_RATE_500HZ     (uint32_t)500

#define SENSOR_CONFIG_OUTPUT_RANGE_180      180
#define SENSOR_CONFIG_OUTPUT_RANGE_360      360

#define LPMS_DISABLE_GYRO_AUTOCAL           0
#define LPMS_ENABLE_GYRO_AUTOCAL            1

// Verbose level
enum {
    VERBOSE_NONE,
    VERBOSE_INFO,
    VERBOSE_DEBUG
};

// Structure for data exchange inside the OpenMAT network.
struct LpmsNAVData
{
    float timestamp;
    float gAngle;
    float gRate;
    float acc[3];

    LpmsNAVData()
    {
        timestamp = 0.0f;
        gAngle = 0.0f;
        gRate = 0.0f;
        acc[0] = 0.0f;
        acc[1] = 0.0f;
        acc[2] = 0.0f;
    }
};

struct LpmsNAVInfo
{
    std::string hardwareVersion;
    std::string firmwareVersion;
    std::string filterVersion;
    std::string serialNumber;
    std::string sensorModel;
    int iapStatus;
    int sensorId;
    int streamFreq;
    bool autoCalStatus;
    float startUpStaticTime;
    float dt;
    int baudrate;
    int outputRange;
    bool ledEnabled;

    LpmsNAVInfo() {
        reset();
    }

    void reset()
    {
        hardwareVersion = "NA";
        firmwareVersion = "NA";
        filterVersion = "NA";
        serialNumber = "NA";
        sensorModel = "NA";
        iapStatus = 0;
        sensorId = 1;
        streamFreq = 0;
        autoCalStatus = false;
        startUpStaticTime = 0;
        dt = 0;
        baudrate = 0;
        outputRange = 0;
        ledEnabled = false;
    }
};

class LpmsNAVI
{
public:
    virtual std::string getVersion() = 0;

    ~LpmsNAVI(void) {};
    virtual void release() = 0;

    virtual void setSensorType(int type) = 0;
    /**
    * Set sensor connection baudrate
    */
    virtual void setPCBaudrate(int baud) = 0;

#ifdef _WIN32
    /**
    * Set sensor connection COM Port
    * @param: eg. 10 for COM10
    */
    virtual void setPCPort(int port) = 0;
#else
    virtual void setPCPort(std::string port) = 0;
#endif
    
    /**
    * Initiate sensor connection
    * return true on success, false for unsuccessful connection
    */
    virtual int connect(void) = 0;

    /**
    * Disconnect sensor
    * return true on success, false for unsuccessful connection
    */
    virtual bool disconnect(void) = 0;

    /**
    * Check sensor connection
    */
    virtual bool isConnected(void) = 0;

    /**
    * Get sensor info
    */
    virtual LpmsNAVInfo getSensorInfo(void) = 0;

    /**
    * Reset sensor heading angle
    */
    virtual bool resetHeading(void) = 0;

    /**
    * Recalibrate sensor gyro static bias
    */
    virtual bool resetGyroStaticBias(void) = 0;

    /**
    * Get current data streaming frequency
    */
    virtual float getDataFrequency(void) = 0;

    /**
    * Set sensor baudrate
    * @param: SENSOR_CONFIG_UART_BAUDRATE_X 
    */
    virtual bool setBaudrate(int baud) = 0;
    /**
    * Get current baudrate settings
    */
    virtual int getBaudrate(void) = 0;
    /**
    * Set sensor output range
    * @param: SENSOR_CONFIG_OUTPUT_RANGE_X 
    */
    virtual bool setOutputRange(int range) = 0;
    /**
    * Get current output range settings
    */
    virtual int getOutputRange(void) = 0;

    /**
    * Set sensor streaming frequency
    * @param: SENSOR_CONFIG_OUTPUT_RATE_X 
    */
    virtual bool setStreamFrequency(int freq) = 0;

    /**
    * Get current streaming frequency settings
    */
    virtual int getStreamFrequency(void) = 0;


    /**
    * Enable sensor LED
    */
    virtual bool enableLed(void) = 0;

    /**
    * Disable sensor LED
    */
    virtual bool disableLed(void) = 0;


    /**
    * Check if data available in sensor data queue
    * return numbe of data in sensor data queue
    */
    virtual int hasData(void) = 0;

    /**
    * Get data in front of sensor data queue
    * return true is data queue size is > 0
    * return false if not data available in data queue
    */
    virtual bool getSensorData(LpmsNAVData &sd) = 0;

    /**
    * Set internal data queue size (default to 10)
    */
    virtual void setSensorDataQueueSize(unsigned int size) = 0;

    /**
    * Get current data queue size (default: 10)
    */
    virtual int getSensorDataQueueSize(void) = 0;

    /**
    * send get sensor data command
    * return true on success, false for unsuccessful connection
    */
    virtual bool sendGetSensorDataCommand(void) = 0;

    /**
    * Get autoReconnect value
    */
    virtual bool getAutoReconnect(void) = 0;

    /**
    * Set autoReconnect value
    */
    virtual bool setAutoReconnect(bool value) = 0;

    /**
    * Get timeoutThreshold value
    */
    virtual long long getTimeoutThreshold(void) = 0;

    /**
    * Set timeoutThreshold value
    */
    virtual long long setTimeoutThreshold(long long value) = 0;

    /**
    * Set autocalibration status
*   * Enable: true
*   * Disable: false
    */

    virtual bool setAutoCalStatus(bool enable) = 0;
    /**
    * Get autocalibration status
*   * true: enabled
*   * false: disabled
    */
    virtual bool getAutoCalStatus() = 0;

    /**
    * Set sensor id
    */
    virtual bool setSensorID(int i) = 0;

    /**
    * Get sensor id
    */
    virtual bool getSensorID() = 0;

    /**
    * Set startup static calibration time
    */
    virtual bool setStartUpStaticTime(float dt) = 0;

    /**
    * Get startup static calibration time
*   * data will be available in sensorInfo.startUpStaticTime
    */
    virtual bool getStartUpStaticTime() = 0;

    /**
    * Goto command mode
    */
    virtual bool gotoCommandMode(void) = 0;

    /**
    * Goto streaming mode
    */
    virtual bool gotoStreamingMode(void) = 0;

    /**
    * Reset factory settings
    */
    virtual bool resetFactory(void) = 0;
    /**
    * Set library verbosity output
    * VERBOSE_NONE      0
    * VERBOSE_INFO      1
    * VERBOSE_DEBUG     2
    */
    virtual void setVerbose(int b) = 0;
    /**
    * has new sensor info.
    */
    virtual bool hasInfo(void) = 0;
};

#ifdef _WIN32
    #ifdef DLL_EXPORT
        #define LPMS_NAV_API __declspec(dllexport)
    #else
        #define LPMS_NAV_API __declspec(dllimport)
    #endif

    extern "C" LPMS_NAV_API	LpmsNAVI* APIENTRY LpmsNAVFactory(int portno, int baudrate);

#else
    #define LPMS_NAV_API
    extern "C" LPMS_NAV_API LpmsNAVI* LpmsNAVFactory(std::string portno, int baudrate);
#endif


#endif