
#include <string>

#include "ros/ros.h"
#include "sensor_msgs/Imu.h"
#include "std_srvs/SetBool.h"
#include "std_srvs/Trigger.h"
#include "std_msgs/Bool.h"
#include <tf/transform_datatypes.h>

#include "lpsensor/LpmsNAVI.h"

class LpNavProxy
{
 public:
     // Node handler
    ros::NodeHandle nh, private_nh;
    ros::Timer updateTimer;

    // Publisher
    ros::Publisher imu_pub;
    ros::Publisher rpy_publisher;
    ros::Publisher autocalibration_status_pub;

    // Service
    ros::ServiceServer gyrocalibration_serv;
    ros::ServiceServer resetHeading_serv;
    ros::ServiceServer sendGetSensorDataCommand_serv;
    ros::ServiceServer setAutoCalibrateStatus_serv;

    sensor_msgs::Imu imu_msg;
    geometry_msgs::Vector3 rpy;

    // Parameters
    std::string comportNo;
    int baudrate;
    bool autoReconnect;
    std::string frame_id;
    int rate;
    double timeout_threshold;
    int type;
    bool quit;

    LpNavProxy(ros::NodeHandle h) : 
        nh(h),
        private_nh("~")
    {
        // Get node parameters
        private_nh.param<std::string>("port", comportNo, "/dev/ttyUSB0");
        private_nh.param("baudrate", baudrate, 115200);
        private_nh.param("autoreconnect", autoReconnect, true);
        private_nh.param<std::string>("frame_id", frame_id, "imu");
        private_nh.param("rate", rate, 200);
        private_nh.param("timeout_threshold", timeout_threshold, 5.0);

        // Create LpmsNAV object 
        sensor1 = LpmsNAVFactory(comportNo, baudrate);

        //  Select sensor communication type, default is RS232.
        //  #define LPMS_NAV2                           0
        //  #define LPMS_NAV2_RS485                     1
        type = LPMS_NAV2;
        sensor1->setSensorType(type);

        sensor1->setVerbose(VERBOSE_INFO);
        sensor1->setAutoReconnect(true);
        // Set timeout threshold (us, default = 5000000)
        sensor1->setTimeoutThreshold(timeout_threshold * 1000000);

        ROS_INFO("Settings");
        ROS_INFO("Port: %s", comportNo.c_str());
        ROS_INFO("Baudrate: %d", baudrate);
        ROS_INFO("Auto reconnect: %s", autoReconnect? "Enabled":"Disabled");

        imu_pub = nh.advertise<sensor_msgs::Imu>("data",1);
        rpy_publisher = nh.advertise<geometry_msgs::Vector3>("rpy",1);
        autocalibration_status_pub = nh.advertise<std_msgs::Bool>("is_autocalibration_active", 1, true);

        gyrocalibration_serv = nh.advertiseService("calibrate_gyroscope", &LpNavProxy::calibrateGyroscope, this);
        resetHeading_serv = nh.advertiseService("reset_heading", &LpNavProxy::resetHeading, this);
        sendGetSensorDataCommand_serv = nh.advertiseService("send_getSensorData", &LpNavProxy::sendGetSensorData, this);
        setAutoCalibrateStatus_serv = nh.advertiseService("setAutoCalibrateStatus", &LpNavProxy::setAutoCalibrateStatus, this);

    }

    ~LpNavProxy(void)
    {
        sensor1->disconnect();
        sensor1->release();
    }

    void update(const ros::TimerEvent& te)
    {
        LpmsNAVData sd;

        if (!quit && (sensor1->isConnected() || sensor1->getAutoReconnect()))
        {
            if (sensor1->hasData())
            {
                // Get latest data
                while (sensor1->hasData())
                {
                    sensor1->getSensorData(sd);
                }

                /* Fill the IMU message */

                // Fill the header
                imu_msg.header.stamp = ros::Time::now();
                imu_msg.header.frame_id = frame_id;

                // Fill orientation quaternion
                double yaw = sd.gAngle*3.1415926/180;

                tf::Vector3 axis(0, 0, 1);
                tf::Quaternion q(axis, yaw);
                imu_msg.orientation.w = q.w();
                imu_msg.orientation.x = q.x();
                imu_msg.orientation.y = q.y();
                imu_msg.orientation.z = q.z();

                // Fill angular velocity data
                // - scale from deg/s to rad/s
                imu_msg.angular_velocity.x = 0;
                imu_msg.angular_velocity.y = 0;
                imu_msg.angular_velocity.z = sd.gRate*3.1415926/180;

                // Fill linear acceleration data
                imu_msg.linear_acceleration.x = sd.acc[0]*9.81;
                imu_msg.linear_acceleration.y = sd.acc[1]*9.81;
                imu_msg.linear_acceleration.z = sd.acc[2]*9.81;

                //Fill roll pitch yaw  //rad
                rpy.x = 0;
                rpy.y = 0;
                rpy.z = yaw;

                // Publish the messages
                imu_pub.publish(imu_msg);
                rpy_publisher.publish(rpy);

                if (type == LPMS_NAV2_RS485)
                {
                    sensor1->sendGetSensorDataCommand();
                }

            }
        }
    }

    bool connect()
    {
        ROS_INFO("Waiting for sensor to connect");
        // Connects to sensor
        if (sensor1->connect() == STATUS_CONNECTED)
        {   
            ROS_INFO("Sensor connected");
            return true;
        }

        return false;
    }

    void run(void)
    {
        if (sensor1->isConnected())
        {
            if (type == LPMS_NAV2_RS485)
            {
                sensor1->sendGetSensorDataCommand();
            }
            quit = false;

            // The timer ensures periodic data publishing
            updateTimer = ros::Timer(nh.createTimer(ros::Duration(1.0/rate),
                                                    &LpNavProxy::update,
                                                    this));
        }
        else
        {
            ROS_INFO("Please connect the sensor before runnung");
        }
    }

    bool setStreamFrequency(int freq)
    {
        LpmsNAVInfo info = sensor1->getSensorInfo();
        sensor1->setStreamFrequency(freq);

        bool hasInfo = false;
        int maxCount = 0;
        float sleepDuration = 0.1f;
        do
        {
            ++maxCount;
            ros::Duration(sleepDuration).sleep();
            sleepDuration = 2.0f * sleepDuration;
        } while( !((hasInfo = sensor1->hasInfo()) || (maxCount >= 3)) );

        if (hasInfo)
        {
            info = sensor1->getSensorInfo();
            if (info.streamFreq == freq)
            {
                return true;
            }
        }

        return false;
    }

    ///////////////////////////////////////////////////
    // Service Callbacks
    ///////////////////////////////////////////////////

    // reset heading
    bool resetHeading (std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &res)
    {
        ROS_INFO("reset_heading");
        
        // Send command
        if(sensor1->resetHeading())
        {
            res.success = true;
            res.message = "[Success] Heading reset";
            return true;
        }
        else
        {
            return false;
        }

    }

    // calibrate gyro
    bool calibrateGyroscope (std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &res)
    {
        ROS_INFO("calibrate_gyroscope: Please make sure the sensor is stationary for 4 seconds");

        if(sensor1->resetGyroStaticBias())
        {
            ros::Duration(4).sleep();
            res.success = true;
            res.message = "[Success] Gyroscope calibration procedure completed";
            ROS_INFO("calibrate_gyroscope: Gyroscope calibration procedure completed");
            return true;
        }
        else
        {
            return false;
        }

    }

    // send GetSensorData
    bool sendGetSensorData (std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &res)
    {
	    ROS_INFO("updating sensor data.");
        if(sensor1->sendGetSensorDataCommand())
        {
            res.success = true;
            res.message = "[Success] Command sent";
            ROS_INFO("sensor data updated");
            return true;
        }
        else
        {
            return false;
        }

    }

    void publishIsAutocalibrationActive()
    {
        std_msgs::Bool msg;
        LpmsNAVInfo settings;
        settings = sensor1->getSensorInfo();
        msg.data = settings.autoCalStatus;
        autocalibration_status_pub.publish(msg);
    }

    // set AutoCalibrateStatus
    bool setAutoCalibrateStatus (std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res)
    {
        ROS_INFO("set autocalibration.");

        // clear current settings
        LpmsNAVInfo settings;
        settings = sensor1->getSensorInfo();

        // Send command
        sensor1->setAutoCalStatus(req.data);
        ros::Duration(0.2).sleep();
        sensor1->getAutoCalStatus();
        ros::Duration(0.1).sleep();

        double retryElapsedTime = 0;
        int retryCount = 0;
        while (!sensor1->hasInfo()) 
        {
            ros::Duration(0.1).sleep();
            ROS_INFO("set autocalibration wait.");
            
            retryElapsedTime += 0.1;
            if (retryElapsedTime > 2.0)
            {
                retryElapsedTime = 0;
                sensor1->getAutoCalStatus();
                retryCount++;
            }

            if (retryCount > 5)
                break;
        }
        ROS_INFO("set autocalibration done.");

        // Get settings
        settings = sensor1->getSensorInfo();

        std::string msg;
        if (settings.autoCalStatus == req.data) 
        {
            res.success = true;
            msg.append(std::string("[Success] autocalibration status set to: ") + (settings.autoCalStatus?"True":"False"));
        }
        else 
        {
            res.success = false;
            msg.append(std::string("[Failed] current autocalibration status set to: ") + (settings.autoCalStatus?"True":"False"));
        }

        ROS_INFO("%s", msg.c_str());
        res.message = msg;

        publishIsAutocalibrationActive();
        return res.success;
    }

 private:

    // Access to LPMS data
    LpmsNAVI* sensor1;
};

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "lpms_nav_node");
    ros::NodeHandle nh("imu");

    ros::AsyncSpinner spinner(0);
    spinner.start();

    LpNavProxy lpNav(nh);
    if (!lpNav.connect())
    {
        ROS_ERROR("Error connecting to sensor");
        return 1;
    }
    lpNav.quit = false;

    lpNav.run();
    ros::waitForShutdown();

    lpNav.quit = true;
    return 0;
}
