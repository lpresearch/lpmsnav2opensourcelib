
#include <windows.h>
#include "SerialPort.h"
using namespace std;

Serial::Serial():
    portNo(0),
    connected(false)
{
}

Serial::~Serial()
{
    //Check if we are connected before trying to disconnect
    close();
}

bool Serial::open(int portno, int baudrate)
{
    if (isConnected())
        CloseHandle(this->hSerial);
    portNo = portno;
    stringstream ss;
    ss << "\\\\.\\COM" << portNo;
    string portName = ss.str();
    //We're not yet connected
    this->connected = false;

    //Try to connect to the given port throuh CreateFile
    this->hSerial = CreateFile(portName.c_str(),
        GENERIC_READ | GENERIC_WRITE,
        0,
        NULL,
        OPEN_EXISTING,
        0,//FILE_ATTRIBUTE_NORMAL,
        NULL);

    //Check if the connection was successfull
    if (this->hSerial == INVALID_HANDLE_VALUE)
    {
        /*
        //If not success full display an Error
        if (GetLastError() == ERROR_FILE_NOT_FOUND){

            //Print Error if neccessary
            printf("[Serial] ERROR: Handle was not attached. Reason: %s not available.\n", portName);
        }
        else
        {
            printf("[Serial] ERROR: Invalide handle value\n");
        }
        */
        printf("[Serial] ERROR: Invalide handle value\n");        
        CloseHandle(this->hSerial);
        return false;
    }
    else
    {
        char mode_str[128];
        switch (baudrate)
        {
        case BAUDRATE_4800:
            strcpy(mode_str, "baud=4800");
            break;
        case BAUDRATE_9600:
            strcpy(mode_str, "baud=9600");
            break;
        case BAUDRATE_19200:
            strcpy(mode_str, "baud=19200");
            break;
        case BAUDRATE_38400:
            strcpy(mode_str, "baud=38400");
            break;
        case BAUDRATE_57600:
            strcpy(mode_str, "baud=57600");
            break;
        case BAUDRATE_115200:
            strcpy(mode_str, "baud=115200");
            break;
        case BAUDRATE_230400:
            strcpy(mode_str, "baud=230400");
            break;
        case BAUDRATE_460800:
            strcpy(mode_str, "baud=460800");
            break;
        case BAUDRATE_921600:
            strcpy(mode_str, "baud=921600");
            break;
        }
        strcat(mode_str, " data=8");
        strcat(mode_str, " parity=n");
        strcat(mode_str, " stop=1");
        strcat(mode_str, " dtr=on rts=on");
        DCB port_settings;
        memset(&port_settings, 0, sizeof(port_settings));  /* clear the new struct  */
        port_settings.DCBlength = sizeof(port_settings);

        if (!BuildCommDCBA(mode_str, &port_settings))
        {
            printf("unable to set comport dcb settings\n");
            CloseHandle(this->hSerial);
            return false;
        }
        if (!BuildCommDCBA(mode_str, &port_settings))
        {
            printf("unable to set comport dcb settings\n");
            CloseHandle(this->hSerial);
            return false;
        }

        if (!SetCommState(this->hSerial, &port_settings))
        {
            printf("unable to set comport cfg settings\n");
            CloseHandle(this->hSerial);
            return false;
        }

        COMMTIMEOUTS Cptimeouts;

        Cptimeouts.ReadIntervalTimeout = MAXDWORD;
        Cptimeouts.ReadTotalTimeoutMultiplier = 0;
        Cptimeouts.ReadTotalTimeoutConstant = 0;
        Cptimeouts.WriteTotalTimeoutMultiplier = 100;
        Cptimeouts.WriteTotalTimeoutConstant = 100;

        if (!SetCommTimeouts(this->hSerial, &Cptimeouts))
        {
            printf("unable to set comport time-out settings\n");
            CloseHandle(this->hSerial);
            return false;
        }
    }

    this->connected = true;
    mm.reset();
    return true;
}

bool Serial::close()
{
    if (this->connected)
    {
        //We're no longer connected
        this->connected = false;
        //Close the serial handler
        return CloseHandle(this->hSerial);
    }

    return true;
}

int Serial::readData(unsigned char *buffer, unsigned int nbChar)
{
    if (!isConnected())
        return 0;

    int n;
    ReadFile(this->hSerial, buffer, nbChar, (LPDWORD)((void *)&n), NULL);
  
    if (n > 0) {
        mm.reset();
    }
    /*
    if (mm.measure() > TIMEOUT_WAIT_TIME) {
        close();
    }
    */
    return n;

}


bool Serial::writeData(unsigned char *buffer, unsigned int nbChar)
{
    if (!isConnected())
    {
        std::cout << "dongle not connected\n";
        return false;
    }
    DWORD bytesSend;
    //Try to write the buffer on the Serial port
    if (!WriteFile(this->hSerial, (void *)buffer, nbChar, &bytesSend, 0))
    {
        std::cout << "Write error\n";
        //In case it don't work get comm error and return false
        ClearCommError(this->hSerial, &this->errors, &this->status);

        return false;
    }
    else 
    {
        return true;
    }

}

bool Serial::isConnected()
{
    //Simply return the connection status
    return this->connected;
}
